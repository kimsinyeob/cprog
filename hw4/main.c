#include <stdio.h>

#define N1 10
#define N2 10
#define N  20

void printpoly(int, const int []);

int* multpoly(int dest[],
              int n1, const int poly1[],
              int n2, const int poly2[]);

int main()
{
    int poly1[N1];
    int poly2[N2];
    int poly[N] = {}; // 모두 0으로 초기화

    for (int i=0; i<N1; ++i) scanf("%d", &poly1[i]); // poly1 입력 받기
    for (int i=0; i<N2; ++i) scanf("%d", poly2 + i); // poly2 입력 받기
    multpoly(poly, N1, poly1, N2, poly2); // poly에 poly1과 poly2의 곱을 저장

    printf("  "); printpoly(N1, poly1); printf("\n");
    printf("* "); printpoly(N2, poly2); printf("\n");
    printf("------------------------------------------------------------\n");
    printf("  "); printpoly(N, poly); printf("\n");

    return 0;
}


void printpoly(int n, const int poly[])
{
    // 이 함수를 작성하라 (4점)
    int a = 0;
    int i = 0;
    int c = 0;

    for(a = 0; a < n; a++)
    {
        if(n == 10)
        {
            if(a < n && c != 0 && poly[a] > 0)
            {
                printf(" +");
            }
        }

        if(n == 20)
        {
            if(a < n - 1 && c != 0 && poly[a] > 0)
            {
                printf(" +");
            }
        }
        if(poly[a] > 0)
        {
            if(a == 0)
            {
                printf("%d", poly[a]);
                c++;
            }

            if(a == 1 && poly[a] != 1)
            {
                printf("%dx", poly[a]);
                c++;
            }

            if(a == 1 && poly[a] == 1)
            {
                printf("x");
                c++;
            }

            if(a > 1 && poly[a] != 1)
            {
                printf("%dx^%d", poly[a], a);
                c++;
            }

            if(a > 1 && poly[a] == 1)
            {
                printf("x^%d", a);
                c++;
            }
        }

        if(poly[a] == 0)
        {
            i++;

            if(n == 10 && i == 10)
            {
                printf("0");
                i = 0;
            }

            if(n == 20 && i == 20)
            {
                printf("0");
                i = 0;
            }
        }

        if(poly[a] < 0)
        {

            if(poly[a] < 0 && c != 0)
            {
                printf(" ");
            }

            if(a == 0)
            {
                printf("%d", poly[a]);
                c++;
            }

            if(a == 1 && poly[a] != -1)
            {
                printf("%dx", poly[a]);
                c++;
            }

            if(a == 1 && poly[a] == -1)
            {
                printf("-x");
                c++;
            }

            if(a > 1 && poly[a] != -1)
            {
                printf("%dx^%d", poly[a], a);
                c++;
            }

            if(a > 1 && poly[a] == -1)
            {
                printf("-x^%d", a);
                c++;
            }
        }
    }
}

int* multpoly(int dest[],
              int n1, const int poly1[],
              int n2, const int poly2[])
{
    // 아래는 poly에 poly1을 그대로 복사하는 더미 구현이므로 곱셈을 하도록 수정 (1점)
    int i;
    int j;

    for(i = 0; i < n1; ++i)
    {
        for(j = 0; j < n2; j++)
        {
            dest[i + j] += poly1[i] * poly2[j];
        }
    }

    return dest;
}
