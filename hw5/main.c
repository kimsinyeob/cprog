#include <stdio.h>
#include <stdlib.h>

#define N1 3
#define N2 3
#define N 6

// x,y,z의 최대 차수가 각각 nx,ny,nz인
// 다항식에 대한 삼중 포인터를 받아 출력하는 함수로
// 출력방식은 x의 차수에 대한 내림차순 최우선하며
// x의 차수가 같은 항끼리는 y에 대한 내림차순을 우선하고
// 마지막으로 x와 y의 차수가 둘 다 같을 때는 z에 대한
// 내림차순으로 각 항들을 출력한다.
void printpoly3(int nx, int ny, int nz, int*** p);

// x,y,z의 최대 차수 nx1,ny1,nz1 및 nx2,ny2,nz2인
// 두 다항식에 대한 삼중 포인터 p1와 p2의 곱을
// x,y,z의 최대 차수 각각 nx,ny,nz 까지 저장 가능한
// 삼중 포인터 dest에 저장하는 함수이다.
int*** multpoly3(
         int nx,  int ny,  int nz,  int ***dest,
         int nx1, int ny1, int nz1, int ***p1,
         int nx2, int ny2, int nz2, int ***p2);

int main(void)
{
    int poly1[N1][N1][N1];
    int poly2[N2][N2][N2];
    int poly[N][N][N] = {}; // 모두 0으로 초기화

    for (int i=0; i<N1; ++i)
        for (int j=0; j<N1; ++j)
            for (int k=0; k<N1; ++k)
                scanf("%d", &(poly1[i][j][k])); // x^iy^jz^k의 계수 입력

    for (int i=0; i<N2; ++i)
        for (int j=0; j<N2; ++j)
            for (int k=0; k<N2; ++k)
                scanf("%d", *(*(poly2+i)+j)+k); // x^iy^jz^k의 계수 입력

    // 3차원 배열 poly1에 대응되는 3중 포인터 p1를 동적 할당으로 정의
    int ***p1 = malloc(N1*sizeof(int**));
    // p1이 poly1에 대응되는 3중 포인터가 되도록 마무리하라

    for(int k = 0; k < N1; ++k)
    {
        p1[k] = malloc(3*sizeof(int*));

        for(int d = 0; d < 3; ++d)
        {
            p1[k][d] = poly1[k][d];
        }
    }

    // 3차원 배열 poly2에 대응되는 3중 포인터 p2를 동적 할당으로 정의
    int ***p2 = malloc(N2*sizeof(int**));
    // p2가 poly2에 대응되는 3중 포인터가 되도록 마무리하라

    for(int k = 0; k < N2; ++k)
    {
        p2[k] = malloc(3*sizeof(int*));

        for(int d = 0; d < 3; ++d)
            {
                p2[k][d] = poly2[k][d];
            }
    }

    // 3차원 배열 poly에 대응되는 3중 포인터 p를 동적 할당으로 정의
    int ***p = malloc(N*sizeof(int**));
    // p가 poly에 대응되는 3중 포인터가 되도록 마무리하라

    for(int k = 0; k < N; ++k)
    {
        p[k] = malloc(6*sizeof(int*));

        for(int d = 0; d < 6; ++d)
        {
            p[k][d] = poly[k][d];
        }
    }

    multpoly3(N,N,N,p, N1,N1,N1,p1, N2,N2,N2,p2);

    printf("  "); printpoly3(N1,N1,N1,p1); printf("\n");
    printf("* "); printpoly3(N2,N2,N2,p2); printf("\n");
    printf("------------------------------------------------------------\n");
    printf("  "); printpoly3(N,N,N,p); printf("\n");

    return 0;
}

void printpoly3(int nx, int ny, int nz, int*** p)
{
    // 이 함수를 작성하라 (5점)
    int a,b,c;
    int i = 0;
    int j = 0;

    for(c = 0; c < nx; c++){
    for(b = 0; b < ny; b++){
    for(a = 0; a < nz; a++){

        if(nx == 3 && ny == 3 && nz == 3)
        {
            if(j != 0 && p[c][b][a] < 0)
            {
                printf(" ");
            }
        }

        if(nx == 6 && ny == 6 && nz == 6)
        {
            if(j != 0 && p[c][b][a] > 0)
            {
               printf(" +");
            }

            if(j != 0 && p[c][b][a] < 0)
            {
                printf(" ");
            }
        }

            if(p[c][b][a] != 0 && nx == 3 && ny == 3 && nz == 3)
            {
                if(c == 0 && b == 0 && a == 0)//1
                {
                    if(p[c][b][a] != 0){
                    printf("%d", p[c][b][a]);
                    j++; }
                }

                if(c == 0 && b == 0 && a == 1)//2
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +z");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-z");
                    j++; }
                }

                if(c == 0 && b == 0 && a == 2)//3
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +z^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-z^2");
                    j++; }

                }

                if(c == 0 && b == 1 && a == 0)//4
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dy", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dy", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dy", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +y");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("y");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y");
                    j++; }
                }

                if(c == 0 && b == 1 && a == 1)//5
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dyz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dyz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dyz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +yz");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("yz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-yz");
                    j++; }
                }

                if(c == 0 && b == 1 && a == 2)//6
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dyz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dyz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dyz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +yz^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("yz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-yz^2");
                    j++; }
                }

                if(c == 0 && b == 2 && a == 0)//7
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dy^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dy^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dy^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +y^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("y^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^2");
                    j++; }
                }

                 if(c == 0 && b == 2 && a == 1)//8
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dy^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dy^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dy^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +y^2z");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("y^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^2z");
                    j++; }
                }

                 if(c == 0 && b == 2 && a == 2)//9
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dy^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dy^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dy^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +y^2z^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("y^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^2z^2");
                    j++; }
                }

                if(c == 1 && b == 0 && a == 0)//10
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dx", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dx", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dx", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x");
                    j++; }
                }

                if(c == 1 && b == 0 && a == 1)//11
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dxz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dxz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dxz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +xz");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("xz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xz");
                    j++; }
                }

                if(c == 1 && b == 0 && a == 2)//12
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dxz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dxz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dxz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +xz^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("xz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xz^2");
                    j++; }
                }

                if(c == 1 && b == 1 && a == 0)//13
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dxy", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dxy", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dxy", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +xy");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("xy");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy");
                    j++; }
                }

                if(c == 1 && b == 1 && a == 1)//14
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dxyz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dxyz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dxyz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j !=0){
                    printf(" +xyz");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("xyz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xyz");
                    j++; }
                }

                if(c == 1 && b == 1 && a == 2)//15
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dxyz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dxyz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dxyz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +xyz^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("xyz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xyz^2");
                    j++; }
                }

                if(c == 1 && b == 2 && a == 0)//16
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dxy^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dxy^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dxy^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +xy^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("xy^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^2");
                    j++; }
                }

                if(c == 1 && b == 2 && a == 1)//17
                {
                    if(p[c][b][a] > 1  && j != 0){
                    printf(" +%dxy^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1  && j == 0){
                    printf("%dxy^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dxy^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +xy^2z");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("xy^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^2z");
                    j++; }
                }

                if(c == 1 && b == 2 && a == 2)//18
                {
                    if(p[c][b][a] > 1  && j != 0){
                    printf(" +%dxy^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dxy^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dxy^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +xy^2z^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("xy^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^2z^2");
                    j++; }
                }

                if(c == 2 && b == 0 && a == 0)//19
                {
                    if(p[c][b][a] > 1  && j != 0){
                    printf(" +%dx^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1  && j == 0){
                    printf("%dx^2", p[c][b][a]);
                    j++; }


                    if(p[c][b][a] < -1){
                    printf("%dx^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2");
                    j++; }
                }

                if(c == 2 && b == 0 && a == 1)//20
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dx^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dx^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dx^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x^2z");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2z");
                    j++; }
                }

                if(c == 2 && b == 0 && a == 2)//21
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dx^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dx^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dx^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x^2z^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2z^2");
                    j++; }
                }

                if(c == 2 && b == 1 && a == 0)//22
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dx^2y", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dx^2y", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dx^2y", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x^2y");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x^2y");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y");
                    j++; }
                }

                if(c == 2 && b == 1 && a == 1)//23
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dx^2yz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dx^2yz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dx^2yz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x^2yz");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x^2yz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2yz");
                    j++; }
                }

                if(c == 2 && b == 1 && a == 2)//24
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dx^2yz^2", p[c][b][a]);
                    j++; }

                     if(p[c][b][a] > 1 && j == 0){
                    printf("%dx^2yz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dx^2yz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x^2yz^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x^2yz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2yz^2");
                    j++; }
                }

                if(c == 2 && b == 2 && a == 0)//25
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dx^2y^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dx^2y^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dx^2y^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x^2y^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x^2y^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^2");
                    j++; }
                }

                if(c == 2 && b == 2 && a == 1)//26
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dx^2y^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dx^2y^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dx^2y^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x^2y^2z");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x^2y^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^2z");
                    j++; }
                }

                if(c == 2 && b == 2 && a == 2)//27
                {
                    if(p[c][b][a] > 1 && j != 0){
                    printf(" +%dx^2y^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] > 1 && j == 0){
                    printf("%dx^2y^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] < -1){
                    printf("%dx^2y^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1 && j != 0){
                    printf(" +x^2y^2z^2");
                    j++; }

                    if(p[c][b][a] == 1 && j == 0){
                    printf("x^2y^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^2z^2");
                    j++; }
                }
            }
            //-----------------------------------------------------------------------------------------------------------//

            if(p[c][b][a] != 0 && nx == 6 && ny == 6 && nz == 6)
            {
                if(c == 0 && b == 0 && a == 0)//1
                {
                    if(p[c][b][a] != 0){
                    printf("%d", p[c][b][a]);
                    j++; }
                }

                if(c == 0 && b == 0 && a == 1)//2
                {
                    if(p[c][b][a] != 0 && p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-z");
                    j++; }
                }

                if(c == 0 && b == 0 && a == 2)//3
                {
                    if(p[c][b][a] != 0 && p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-z^2");
                    j++; }

                }

                if(c == 0 && b == 0 && a == 3)//4
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dz^3", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-z^3");
                    j++; }
                }

                if(c == 0 && b == 0 && a == 4)//5
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dz^4", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-z^4");
                    j++; }
                }

                if(c == 0 && b == 1 && a == 0)//6
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y");
                    j++; }
                }

                if(c == 0 && b == 1 && a == 1)//7
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dyz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("yz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-yz");
                    j++; }
                }

                 if(c == 0 && b == 1 && a == 2)//8
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dyz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("yz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-yz^2");
                    j++; }
                }

                 if(c == 0 && b == 1 && a == 3)//9
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dyz^3", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("yz^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-yz^3");
                    j++; }
                }

                if(c == 0 && b == 1 && a == 4)//10
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dyz^4", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("yz^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-yz^4");
                    j++; }
                }

                if(c == 0 && b == 2 && a == 0)//11
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^2");
                    j++; }
                }

                if(c == 0 && b == 2 && a == 1)//12
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^2z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^2z");
                    j++; }
                }

                if(c == 0 && b == 2 && a == 2)//13
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^2z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^2z^2");
                    j++; }
                }

                if(c == 0 && b == 2 && a == 3)//14
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^2z^3", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^2z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^2z^3");
                    j++; }
                }

                if(c == 0 && b == 2 && a == 4)//15
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^2z^4", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^2z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^2z^4");
                    j++; }
                }

                if(c == 0 && b == 3 && a == 0)//16
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^3", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^3");
                    j++; }
                }

                if(c == 0 && b == 3 && a == 1)//17
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^3z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^3z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^3z");
                    j++; }
                }

                if(c == 0 && b == 3 && a == 2)//18
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^3z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^3z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^3z^2");
                    j++; }
                }

                if(c == 0 && b == 3 && a == 3)//19
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^3z^3", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^3z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^3z^3");
                    j++; }
                }

                if(c == 0 && b == 3 && a == 4)//20
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^3z^4", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^3z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^3z^4");
                    j++; }
                }

                if(c == 0 && b == 4 && a == 0)//21
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^4", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^4");
                    j++; }
                }

                if(c == 0 && b == 4 && a == 1)//22
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^4z", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^4z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^4z");
                    j++; }
                }

                if(c == 0 && b == 4 && a == 2)//23
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^4z^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^4z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^4z^2");
                    j++; }
                }

                if(c == 0 && b == 4 && a == 3)//24
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^4z^3", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^4z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^4z^3");
                    j++; }
                }

                if(c == 0 && b == 4 && a == 4)//25
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dy^4z^4", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("y^4z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-y^4z^4");
                    j++; }
                }

                if(c == 1 && b == 0 && a == 0)//26
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("x");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x");
                    j++; }
                }

                if(c == 1 && b == 0 && a == 1)//27
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxz", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("xz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xz");
                    j++; }
                }

                if(c == 1 && b == 0 && a == 2)//28
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxz^2", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("xz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xz^2");
                    j++; }
                }

                if(c == 1 && b == 0 && a == 3)//29
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxz^3", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("xz^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xz^3");
                    j++; }

                }

                if(c == 1 && b == 0 && a == 4)//30
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxz^4", p[c][b][a]);
                    j++; }

                    if(p[c][b][a] == 1){
                    printf("xz^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xz^4");
                    j++; }
                }

                if(c == 1 && b == 1 && a == 0)//31
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy");
                    j++; }
                }

                if(c == 1 && b == 1 && a == 1)//32
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxyz", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xyz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xyz");
                    j++; }
                }

                if(c == 1 && b == 1 && a == 2)//33
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxyz^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xyz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xyz^2");
                    j++; }
                }

                 if(c == 1 && b == 1 && a == 3)//34
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxyz^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xyz^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xyz^3");
                    j++; }
                }

                 if(c == 1 && b == 1 && a == 4)//35
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxyz^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xyz^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xyz^4");
                    j++; }
                }

                if(c == 1 && b == 2 && a == 0)//36
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^2");
                    j++; }
                }

                if(c == 1 && b == 2 && a == 1)//37
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^2z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^2z");
                    j++; }
                }

                if(c == 1 && b == 2 && a == 2)//38
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^2z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^2z^2");
                    j++; }
                }

                if(c == 1 && b == 2 && a == 3)//39
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^2z^3", p[c][b][a]);
                    j++; }


                    if(p[c][b][a] == 1){
                    printf("xy^2z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^2z^3");
                    j++; }
                }

                if(c == 1 && b == 2 && a == 4)//40
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^2z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^2z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^2z^4");
                    j++; }
                }

                if(c == 1 && b == 3 && a == 0)//41
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^3");
                    j++; }
                }

                if(c == 1 && b == 3 && a == 1)//42
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^3z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^3z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^3z");
                    j++; }
                }

                if(c == 1 && b == 3 && a == 2)//43
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^3z^2", p[c][b][a]);
                    j++; }


                    if(p[c][b][a] == 1){
                    printf("xy^3z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^3z^2");
                    j++; }
                }

                if(c == 1 && b == 3 && a == 3)//44
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^3z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^3z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^3z^3");
                    j++; }
                }

                if(c == 1 && b == 3 && a == 4)//45
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^3z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^3z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^3z^4");
                    j++; }
                }

                if(c == 1 && b == 4 && a == 0)//46
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^4");
                    j++; }
                }

                if(c == 1 && b == 4 && a == 1)//47
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^4z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^4z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^4z");
                    j++; }
                }

                if(c == 1 && b == 4 && a == 2)//48
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^4z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^4z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^4z^2");
                    j++; }
                }

                if(c == 1 && b == 4 && a == 3)//49
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^4z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^4z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^4z^3");
                    j++; }
                }

                if(c == 1 && b == 4 && a == 4)//50
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dxy^4z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("xy^4z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-xy^4z^4");
                    j++; }
                }

                if(c == 2 && b == 0 && a == 0)//51
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2");
                    j++; }
                }

                if(c == 2 && b == 0 && a == 1)//52
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2z");
                    j++; }
                }

                if(c == 2 && b == 0 && a == 2)//53
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2z^2");
                    j++; }
                }

                if(c == 2 && b == 0 && a == 3)//54
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2z^3");
                    j++; }
                }

                if(c == 2 && b == 0 && a == 4)//55
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2z^4");
                    j++; }
                }

                if(c == 2 && b == 1 && a == 0)//56
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y");
                    j++; }
                }

                if(c == 2 && b == 1 && a == 1)//57
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2yz", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2yz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2yz");
                    j++; }
                }

                if(c == 2 && b == 1 && a == 2)//58
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2yz^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2yz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2yz^2");
                    j++; }
                }

                if(c == 2 && b == 1 && a == 3)//59
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2yz^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2yz^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2yz^3");
                    j++; }
                }

                if(c == 2 && b == 1 && a == 4)//60
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2yz^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2yz^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2yz^4");
                    j++; }
                }

                if(c == 2 && b == 2 && a == 0)//61
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^2");
                    j++; }
                }

                if(c == 2 && b == 2 && a == 1)//62
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^2z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^2z");
                    j++; }
                }

                if(c == 2 && b == 2 && a == 2)//63
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^2z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^2z^2");
                    j++; }
                }

                if(c == 2 && b == 2 && a == 3)//64
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^2z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^2z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^2z^3");
                    j++; }
                }

                if(c == 2 && b == 2 && a == 4)//65
                {
                    if(p[c][b][a] != 0 && p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^2z^4", p[c][b][a]);
                    j++; }


                    if(p[c][b][a] == 1){
                    printf("x^2y^2z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^2z^4");
                    j++; }
                }

                if(c == 2 && b == 3 && a == 0)//66
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^3");
                    j++; }
                }

                if(c == 2 && b == 3 && a == 1)//67
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^3z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^3z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^3z");
                    j++; }
                }

                if(c == 2 && b == 3 && a == 2)//68
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^3z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^3z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^3z^2");
                    j++; }
                }

                if(c == 2 && b == 3 && a == 3)//69
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^3z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^3z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^3z^3");
                    j++; }
                }

                if(c == 2 && b == 3 && a == 4)//70
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^3z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^3z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^3z^4");
                    j++; }
                }

                if(c == 2 && b == 4 && a == 0)//71
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^4");
                    j++; }
                }

                if(c == 2 && b == 4 && a == 1)//72
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^4z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^4z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^4z");
                    j++; }
                }

                if(c == 2 && b == 4 && a == 2)//73
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^4z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^4z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^4z^2");
                    j++; }
                }

                if(c == 2 && b == 4 && a == 3)//74
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^4z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^4z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^4z^3");
                    j++; }
                }

                if(c == 2 && b == 4 && a == 4)//75
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^2y^4z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^2y^4z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^2y^4z^4");
                    j++; }
                }

                if(c == 3 && b == 0 && a == 0)//76
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3");
                    j++; }
                }

                if(c == 3 && b == 0 && a == 1)//77
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3z");
                    j++; }
                }

                if(c == 3 && b == 0 && a == 2)//78
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3z^2");
                    j++; }
                }

                if(c == 3 && b == 0 && a == 3)//79
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3z^3");
                    j++; }
                }

                if(c == 3 && b == 0 && a == 4)//80
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3z^4");
                    j++; }
                }

                if(c == 3 && b == 1 && a == 0)//81
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y");
                    j++; }
                }

                if(c == 3 && b == 1 && a == 1)//82
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3yz", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3yz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3yz");
                    j++; }
                }

                if(c == 3 && b == 1 && a == 2)//83
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3yz^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3yz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3yz^2");
                    j++; }
                }

                if(c == 3 && b == 1 && a == 3)//84
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3yz^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3yz^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3yz^3");
                    j++; }
                }

                if(c == 3 && b == 1 && a == 4)//85
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3yz^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3yz^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3yz^4");
                    j++; }
                }

                if(c == 3 && b == 2 && a == 0)//86
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^2");
                    j++; }
                }

                if(c == 3 && b == 2 && a == 1)//87
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^2z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^2z");
                    j++; }
                }

                if(c == 3 && b == 2 && a == 2)//88
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^2z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^2z^2");
                    j++; }
                }

                if(c == 3 && b == 2 && a == 3)//89
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^2z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^2z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^2z^3");
                    j++; }
                }

                if(c == 3 && b == 2 && a == 4)//90
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^2z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^2z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^2z^4");
                    j++; }
                }

                if(c == 3 && b == 3 && a == 0)//91
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^3");
                    j++; }
                }

                if(c == 3 && b == 3 && a == 1)//92
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^3z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^3z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^3z");
                    j++; }
                }

                if(c == 3 && b == 3 && a == 2)//93
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^3z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^3z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^3z^2");
                    j++; }
                }

                if(c == 3 && b == 3 && a == 3)//94
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^3z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^3z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^3z^3");
                    j++; }
                }

                if(c == 3 && b == 3 && a == 4)//95
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^3z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^3z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^3z^4");
                    j++; }
                }

                if(c == 3 && b == 4 && a == 0)//96
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^4");
                    j++; }
                }

                if(c == 3 && b == 4 && a == 1)//97
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^4z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^4z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^4z");
                    j++; }
                }

                if(c == 3 && b == 4 && a == 2)//98
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^4z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^4z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^4z^2");
                    j++; }
                }

                if(c == 3 && b == 4 && a == 3)//99
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^4z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^4z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^4z^3");
                    j++; }
                }

                if(c == 3 && b == 4 && a == 4)//100
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^3y^4z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^3y^4z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^3y^4z^4");
                    j++; }
                }

                if(c == 4 && b == 0 && a == 0)//101
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4");
                    j++; }
                }

                if(c == 4 && b == 0 && a == 1)//102
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4z");
                    j++; }
                }

                if(c == 4 && b == 0 && a == 2)//103
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4z^2");
                    j++; }
                }

                if(c == 4 && b == 0 && a == 3)//104
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4z^3");
                    j++; }
                }

                if(c == 4 && b == 0 && a == 4)//105
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4z^4");
                    j++; }
                }

                if(c == 4 && b == 1 && a == 0)//106
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y");
                    j++; }
                }

                if(c == 4 && b == 1 && a == 1)//107
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4yz", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4yz");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4yz");
                    j++; }
                }

                if(c == 4 && b == 1 && a == 2)//108
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4yz^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4yz^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4yz^2");
                    j++; }
                }

                if(c == 4 && b == 1 && a == 3)//109
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4yz^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4yz^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4yz^3");
                    j++; }
                }

                if(c == 4 && b == 1 && a == 4)//110
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4yz^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4yz^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4yz^4");
                    j++; }
                }

                if(c == 4 && b == 2 && a == 0)//111
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^2");
                    j++; }
                }

                if(c == 4 && b == 2 && a == 1)//112
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^2z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^2z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^2z");
                    j++; }
                }

                if(c == 4 && b == 2 && a == 2)//113
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^2z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^2z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^2z^2");
                    j++; }
                }

                if(c == 4 && b == 2 && a == 3)//114
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^2z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^2z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^2z^3");
                    j++; }
                }

                if(c == 4 && b == 2 && a == 4)//115
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^2z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^2z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^2z^4");
                    j++; }
                }

                if(c == 4 && b == 3 && a == 0)//116
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^3");
                    j++; }
                }

                if(c == 4 && b == 3 && a == 1)//117
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^3z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^3z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^3z");
                    j++; }
                }

                if(c == 4 && b == 3 && a == 2)//118
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^3z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^3z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^3z^2");
                    j++; }
                }

                if(c == 4 && b == 3 && a == 3)//119
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^3z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^3z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^3z^3");
                    j++; }
                }

                if(c == 4 && b == 3 && a == 4)//120
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^3z^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^3z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^3z^4");
                    j++; }
                }

                if(c == 4 && b == 4 && a == 0)//121
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^4", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^4");
                    j++; }
                }

                if(c == 4 && b == 4 && a == 1)//122
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^4z", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^4z");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^4z");
                    j++; }
                }

                if(c == 4 && b == 4 && a == 2)//123
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^4z^2", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^4z^2");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^4z^2");
                    j++; }
                }

                if(c == 4 && b == 4 && a == 3)//124
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^4z^3", p[c][b][a]);
                    j++; }



                    if(p[c][b][a] == 1){
                    printf("x^4y^4z^3");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^4z^3");
                    j++; }
                }

                if(c == 4 && b == 4 && a == 4)//125
                {
                    if(p[c][b][a] != 0&& p[c][b][a] != -1 && p[c][b][a] != 1){
                    printf("%dx^4y^4z^4", p[c][b][a]);
                    j++; }


                    if(p[c][b][a] == 1){
                    printf("x^4y^4z^4");
                    j++; }

                    if(p[c][b][a] == -1){
                    printf("-x^4y^4z^4");
                    j++; }
                }
            }


            if(p[c][b][a] == 0)
            {
                i++;

                if(nx == 3 && ny == 3 && nz == 3 && i == 27)
                {
                    printf("0");
                    i = 0;
                }

                if(nx == 6 && ny == 6 && nz == 6 && i == 216)
                {
                    printf("0");
                    i = 0;
                }
            }
    }
    }
    }
}

int*** multpoly3(
         int nx,  int ny,  int nz,  int ***dest,
         int nx1, int ny1, int nz1, int ***p1,
         int nx2, int ny2, int nz2, int ***p2)
{
    // 이 함수를 작성하라 (1점)

    int a = 0;
    int b = 0;
    int c = 0;
    int d,e,f,g,h,i;

                for(d = 0; d < nx1; d++)
                {
                    for(e = 0; e < ny1; e++)
                    {
                        for(f = 0; f < nz1; f++)
                        {
                            for(g = 0; g < nx2; g++)
                            {
                                for(h = 0; h < ny2; h++)
                                {
                                    for(i = 0; i < nz2; i++)
                                    {
                                        dest[d+g][e+h][f+i] += p1[d][e][f] * p2[g][h][i];
                                    }
                                }
                            }
                        }
                    }
                }

    return dest;
}
